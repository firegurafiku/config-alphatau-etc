<gconfschemafile>
    <schemalist>

      <schema>
      <key>/schemas/apps/gksu/disable-grab</key>
      <applyto>/apps/gksu/disable-grab</applyto>
      <owner>gksu</owner>
      <type>bool</type>
      <default>false</default>
      <locale name="C">
	<short>Disable keyboard and mouse grab</short>
	<long>
	Whether the keyboard and mouse grabbing should be turned
	off. This will make it possible for other X applications to
	listen to keyboard input events, thus making it not possible
	to shield from malicious applications which may be running.
	</long>
      </locale>

      <locale name="es">
	<short>Desactivar el bloqueo del teclado y el ratón</short>
	<long>Si el bloqueo del teclado y el ratón esté desactivado, otras aplicaciones de X podrán escuchar los eventos de entrada del teclado, incluso no pudiendo protegerse de aplicaciones malignas que pueden estar ejecutándose.</long>
      </locale>
      </schema>

      <schema>
      <key>/schemas/apps/gksu/force-grab</key>
      <applyto>/apps/gksu/force-grab</applyto>
      <owner>gksu</owner>
      <type>bool</type>
      <default>false</default>
      <locale name="C">
	<short>Force keyboard and mouse grab</short>
	<long>
	Grab keyboard and mouse even if -g has been passed as argument
	on the command line.
	</long>
      </locale>

      <locale name="es">
	<short>Forzar el bloqueo del teclado y el ratón</short>
	<long>Bloquear el teclado y el ratón incluso si se ha pasado la opción «-g» en la línea de órdenes.</long>
      </locale>
      </schema>

      <schema>
      <key>/schemas/apps/gksu/sudo-mode</key>
      <applyto>/apps/gksu/sudo-mode</applyto>
      <owner>gksu</owner>
      <type>bool</type>
      <default>false</default>
      <locale name="C">
	<short>Sudo mode</short>
	<long>
	Whether sudo should be the default backend method. This method
        is otherwise accessed though the -S switch or by running 'gksudo'
        instead of 'gksu'.
	</long>
      </locale>

      <locale name="es">
	<short>Modo de sudo</short>
	<long>Cuando sudo sea el método predeterminado, a este método se accederá mediante el parámetro «-S» o ejecutando «gksudo» en lugar de «gksu».</long>
      </locale>
      </schema>

      <schema>
      <key>/schemas/apps/gksu/prompt</key>
      <applyto>/apps/gksu/prompt</applyto>
      <owner>gksu</owner>
      <type>bool</type>
      <default>false</default>
      <locale name="C">
	<short>Prompt for grabbing</short>
	<long>
	This option will make gksu prompt the user if he wants to have
	the screen grabbed before entering the password. Notice that this
	only has an effect if force-grab is disabled.
	</long>
      </locale>

      <locale name="es">
	<short>Preguntar al bloquear</short>
	<long>Esta opción hará que gksu pregunte al usuario si quiere tener la pantalla bloqueada antes de introducir la contraseña. Tenga en cuenta que esto sólo tiene efecto si no fuerza el bloqueo.</long>
      </locale>
      </schema>

      <schema>
      <key>/schemas/apps/gksu/display-no-pass-info</key>
      <applyto>/apps/gksu/display-no-pass-info</applyto>
      <owner>gksu</owner>
      <type>bool</type>
      <default>true</default>
      <locale name="C">
	<short>Display information message when no password is needed</short>
	<long>
	This option determines whether a message dialog will be displayed
	informing the user that the program is being run without the need
	of a password being asked for some reason.
	</long>
      </locale>

      <locale name="es">
	<short>Mostrar el mensaje de información cuando no se necesite la contraseña</short>
	<long>Esta opción determina si un diálogo de mensajes se mostrará informando al usuario de que el programa se está ejecutando sin necesidad de pedirle una contraseña por alguna razón.</long>
      </locale>
      </schema>

      <schema>
      <key>/schemas/apps/gksu/save-to-keyring</key>
      <applyto>/apps/gksu/save-to-keyring</applyto>
      <owner>gksu</owner>
      <type>bool</type>
      <default>true</default>
      <locale name="C">
	<short>Save password to gnome-keyring</short>
	<long>
	gksu can save the password you type to the gnome-keyring so you'll
	not be asked everytime
	</long>
      </locale>

      <locale name="es">
	<short>Guardar la contraseña en el depósito de claves de GNOME («gnome-keyring»)</short>
	<long>gksu puede guardar la contraseña que ha escrito en el depósito de claves de GNOME («gnome-keyring»). de modo que no se le pregunte siempre por ella</long>
      </locale>
      </schema>

      <schema>
      <key>/schemas/apps/gksu/save-keyring</key>
      <applyto>/apps/gksu/save-keyring</applyto>
      <owner>gksu</owner>
      <type>string</type>
      <default>session</default>
      <locale name="C">
	<short>Keyring to which passwords will be saved</short>
	<long>
	The name of the keyring gksu should use. Usual values are "session",
	which saves the password for the session, and "default", which saves the
	password with no timeout.
	</long>
      </locale>

      <locale name="es">
	<short>Depósito de claves en el que se guardarán las contraseñas</short>
	<long>El nombre del depósito de claves que debería utilizar gksu. Los valores usuales son «sesión», que guarda la contraseña durante la sesión, y «predeterminado», que guarda la contraseña sin fecha de caducidad.</long>
      </locale>
      </schema>

  </schemalist>  
</gconfschemafile>
